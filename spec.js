// const { browser } = require("protractor");

const { browser, By } = require("protractor");
const xlsx = require('xlsx');
const fs = require("fs");
const EC = protractor.ExpectedConditions;

describe("Protractor Demo App", () => {
  jasmine.DEFAULT_TIMEOUT_INTERVAL = 90000;
  browser.ignoreSynchronization = true;

  function excel(sheetname, cell) {
    var workbook = xlsx.readFile('test.xlsx');
    var worksheet = workbook.Sheets[sheetname];
    var cell_object = worksheet[cell];

    var val = cell_object.v
    console.log(val);

    return val
  }

  function WriteExcel(sheetname, cell, message) {
    var workbook = xlsx.writeFile('test.xlsx');
    var worksheet = workbook.Sheets[sheetname];
    var cell_object = worksheet[cell];
    var val = cell_object.v
    console.log(val);

    return val
  }

  //   var workbook = xlsx.readFile('test.xlsx');
  //   var worksheet = workbook.Sheets['Sheet1'];
  //   var cell_object = worksheet['A1'];

  //   var val = cell_object.v
  // console.log(val);

  /**Loggingin into URL  */

  it("Login with Credentials", async () => {
    await browser.get(excel('Sheet1', 'A1'))
    // expect(browser.getCurrentUrl()).toBe('http://10.0.12.18:8011/login')
    let title = await browser.getTitle();
    console.log(title);

    /**
     *  Verify and make sure following fields are present on login Page (username, password,login button)
     */

    let username = await browser.findElement(
      By.xpath('//input[@formcontrolname="username"]')
    );
    expect(username.isDisplayed()).toBe(true);
    let password = await browser.findElement(
      By.xpath('//input[@formcontrolname="password"]')
    );
    expect(password.isDisplayed()).toBe(true);
    let loginButton = await browser.findElement(
      By.xpath('//button[@ng-reflect-color="primary"]')
    );
    expect(loginButton.isDisplayed()).toBe(true);

    /**  Validate Login Functionality */

    await browser
      .findElement(By.xpath('//input[@formcontrolname="username"]'))
      .sendKeys(excel("Sheet1", "A2"));

    await browser
      .findElement(By.xpath('//input[@formcontrolname="password"]'))
      .sendKeys(excel("Sheet1", "A3"));

    await browser
      .findElement(By.xpath('//button[@ng-reflect-color="primary"]'))
      .click();

    browser.sleep(5000);

    await browser.wait(() => {
      return browser.wait(EC.urlContains("http://10.0.12.18:8011/hierarchy"));
    });

    let data = await browser
      .findElement(By.xpath('//snack-bar-container[@role="status"]'))
      .getText()
      .then((val) => {
        return val
      });





    //   fs.writeFile('msg.txt',JSON.stringify(data),(err)=>{

    //    if(err) throw err
    //   })


    // browser.sleep(5000);
  });

  /** Create Gratuity */

  // it("Create Gratuity", async () => {
  //   await browser.get("http://10.0.12.18:8011/gratuity");
  //   browser.sleep(5000);

  //   /**
  //    * Verify and make sure Create  button is available
  //    */

  //   let createButton = await browser.findElement(
  //     By.xpath('//fa-icon[@class="ng-fa-icon"]')
  //   );

  //   expect(createButton.isDisplayed()).toBe(true);

  //   /**  Validate Create Gratuity Button functionality  */

  //   await browser
  //     .findElement(By.xpath('//fa-icon[@class="ng-fa-icon"]'))
  //     .click();

  //   /** Verify whether following fields are available */

  //   let nameField = await browser.findElement(
  //     By.xpath('//input[@formcontrolname="Name"]')
  //   );
  //   expect(nameField.isDisplayed()).toBe(true);

  //   let codeField = await browser.findElement(
  //     By.xpath('//input[@formcontrolname="Code"]')
  //   );
  //   expect(codeField.isDisplayed()).toBe(true);

  //   let metricType = await browser.findElement(
  //     By.xpath('//mat-select[@formcontrolname="MetricType"]')
  //   );
  //   expect(metricType.isDisplayed()).toBe(true);

  //   let rate = await browser.findElement(
  //     By.xpath('//input[@formcontrolname="Rate"]')
  //   );
  //   expect(rate.isDisplayed()).toBe(true);

  //   /** Validate Create Gratutiy functionality by providing valid data in all fields */

  //   await browser
  //     .findElement(By.xpath('//input[@formcontrolname="Name"]'))
  //     .sendKeys(excel("Sheet1","A5"));

  //   await browser
  //     .findElement(By.xpath('//input[@formcontrolname="Code"]'))
  //     .sendKeys(excel("Sheet1","A6"));

  //   await browser
  //     .findElement(By.xpath('//input[@formcontrolname="Rate"]'))
  //     .sendKeys(excel("Sheet1","A7"));

  //   await browser
  //     .findElement(By.xpath('//mat-select[@formcontrolname="MetricType"]'))
  //     .click();

  //   await browser
  //     .findElement(By.xpath('//mat-option[@ng-reflect-value="$"]'))
  //     .click();

  //   // await browser.findElement(By.xpath('//mat-option[@ng-reflect-value="%"]')).click()

  //   /**  Verify and Make sure Create Button is available on Create Gratuity Window**/

  //   let createGratuityButton = await browser.findElement(
  //     By.xpath('//button[@color="primary"]')
  //   );
  //   expect(createGratuityButton.isDisplayed()).toBe(true);

  //   /** Validate Create Button functionality on Create Gratuity Window */

  //   await browser.findElement(By.xpath('//button[@color="primary"]')).click();

  //   browser.sleep(5000);

  //   let data = await browser
  //     .findElement(By.xpath('//snack-bar-container[@role="status"]'))
  //     .getText()
  //     .then((val) => {
  //       return val
  //     });

  //     fs.appendFile('msg.txt',JSON.stringify(data),(err)=>{

  //       if(err) throw err
  //      })

  //     await browser.findElement(By.xpath('//a[@type="button"]')).click()
  // });

  // /**Update Gratuity */

  // it("Update Gratuity", async () => {

  //   browser.sleep(5000);
  //   await browser.findElement(By.xpath('//a[@title="Edit"]')).click();
  //   browser.sleep(5000);

  //   await browser
  //     .findElement(By.xpath('//input[@formcontrolname="Name"]'))
  //     .clear();
  //   await browser
  //     .findElement(By.xpath('//input[@formcontrolname="Name"]'))
  //     .sendKeys("XRT123");
  //   await browser
  //     .findElement(By.xpath('//input[@formcontrolname="Rate"]'))
  //     .clear();
  //   await browser
  //     .findElement(By.xpath('//input[@formcontrolname="Rate"]'))
  //     .sendKeys("9");

  //   await browser.findElement(By.xpath('//button[@color="primary"]')).click();
  //   browser.sleep(5000);

  // let data =  await browser
  //     .findElement(By.xpath('//snack-bar-container[@role="status"]'))
  //     .getText()
  //     .then((val) => {
  //       return val
  //     });

  //     fs.appendFile('msg.txt',JSON.stringify(data),(err)=>{

  //       if(err) throw err
  //      })
  // });

  // /** View Gratuity */

  // it("View Gratuity", async () => {
  //   browser.sleep(5000);

  //   await browser.findElement(By.xpath('//a[@title="View"]')).click();
  //   browser.sleep(2000);

  //   await browser.findElement(By.xpath('//a[@type="button"]')).click();
  //   browser.sleep(5000);
  // });

  // /** Invalid data as values */

  // it("Negative data", async () => {

  //   browser.sleep(2000);

  //   await browser
  //     .findElement(By.xpath('//fa-icon[@class="ng-fa-icon"]'))
  //     .click();
  //   browser.sleep(5000);
  //   await browser
  //     .findElement(By.xpath('//input[@formcontrolname="Name"]'))
  //     .sendKeys("@");

  //   await browser
  //     .findElement(By.xpath('//input[@formcontrolname="Code"]'))
  //     .click();

  //  let data = await browser
  //     .findElement(By.xpath('//mat-error[@role="alert"]'))
  //     .getText()
  //     .then((val) => {
  //       return val
  //     });

  //     fs.appendFile('msg.txt',JSON.stringify(data),(err)=>{

  //       if(err) throw err
  //      })

  //     await browser.findElement(By.xpath('//a[@type="button"]')).click()
  // });

  // /** Validate Mandatory Fields */

  // it("Mandatory fields message", async () => {

  //   browser.sleep(5000);

  //   await browser
  //     .findElement(By.xpath('//fa-icon[@class="ng-fa-icon"]'))
  //     .click();

  //   await browser
  //     .findElement(By.xpath('//input[@formcontrolname="Name"]'))
  //     .click();

  //   await browser
  //     .findElement(By.xpath('//input[@formcontrolname="Code"]'))
  //     .click();
  //   browser.sleep(5000);

  // let data =  await browser
  //     .findElement(By.xpath('//mat-error[@role="alert"]'))
  //     .getText()
  //     .then((val) => {
  //       return val
  //     });

  //     fs.appendFile('msg.txt',JSON.stringify(data),(err)=>{

  //       if(err) throw err
  //      })

  //     await browser.findElement(By.xpath('//a[@type="button"]')).click()

  //   browser.sleep(5000);
  // });
});
