const { browser } = require('protractor');
var UpdateGratuity = require('../../page-objects/UpdateGratuity');

describe('Update Gratuity',function(){

    it('As a user, I should update gratuity',function() {

        var updategratuity = new UpdateGratuity;

        updategratuity.get();
        updategratuity.editButton();
        updategratuity.clearName();
        updategratuity.Name('V34');
        updategratuity.clickName();
        updategratuity.clearCode();
        updategratuity.Code('836');
        updategratuity.clickCode();
        updategratuity.clearRate();
        updategratuity.Rate('6');
        updategratuity.updateButton();
        updategratuity.Cancel();
        updategratuity.getGreetings();


    })
})