var mandatoryHelper = function(){

    var create = element(by.xpath('//fa-icon[@class="ng-fa-icon"]'));
    var name = element(by.xpath('//input[@formcontrolname="Name"]'));
    var code = element(by.xpath('//input[@formcontrolname="Code"]'));
    var rate = element(by.xpath('//input[@formcontrolname="Rate"]'));
    var greetings = element(by.xpath('//mat-error[@role="alert"]'));
    var cancel = element(by.cssContainingText('.mat-button-wrapper','Cancel'));

    
//     this.get = async() =>{

//     await browser.get('http://10.0.12.18:8011/gratuity');
//     console.log('Gratuity URL');
//    };

    this.createGratuity = async()=>{

        await create.click();
        console.log('create window');
    };
    this.Name = async()=>{

        await name.click();
        console.log('Username');
    };
    
    this.Code = async()=>{
    
        await code.click();
        console.log('Code');
    };
    
    this.Rate = async()=>{

        await rate.click();
        console.log('Rate');
    };
    this.Name = async()=>{

        await name.click();
        console.log('Username');
    };

    this.getGreetings = async()=>{

        return await greetings.getText().then((val)=>{
          console.log(val);
          
          });
          
      };
      this.Cancel = async()=>{

        await cancel.click();
        console.log('CancelButton');
    };
    
}

module.exports = mandatoryHelper;