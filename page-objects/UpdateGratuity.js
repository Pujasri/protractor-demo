var updateGratuity = function(){

var editbutton = element(by.xpath('//a[@title="Edit"]'));
var name = element(by.xpath('//input[@placeholder="Name"]'));
var code = element(by.xpath('//input[@formcontrolname="Code"]'));
var rate = element(by.xpath('//input[@formcontrolname="Rate"]'));
var updatebutton = element(by.xpath('//button[@color="primary"]'));
var greeting = element(by.xpath('//snack-bar-container[@role="status"]'));
var cancel = element(by.cssContainingText('.mat-button-wrapper','Cancel'));

// this.get = async() =>{

//     await browser.get('http://10.0.12.18:8011/gratuity');
//     console.log('Gratuity URL');
// };

this.editButton = async()=>{

    await editbutton.click();
    console.log('Edit gratuity pop-up');
};

this.clearName = async()=>{

    await name.clear();
    console.log('clear Name');
};

this.Name = async(gName)=>{
    await name.click();

    await name.sendKeys(gName);
    console.log('Updated Name');
};

this.clickName = async()=>{
    await name.click();
}

this.clearCode = async()=>{
    await code.clear();
    console.log('clear code');

};
this.clickCode = async()=>{
    await code.click();
}

this.Code = async(gCode)=>{
    await code.click();
    await code.sendKeys(gCode);
    console.log('Code');
};

this.clearRate = async()=>{
    await rate.clear();
    console.log('clear Rate');

};

this.Rate = async(gRate)=>{
    await rate.click();

    await rate.sendKeys(gRate);
    console.log('Rate');
};

this.updateButton = async()=>{

    await updatebutton.click();
    console.log('update Gratuity');
};

this.getGreetings = async()=>{

    await greeting.getText().then((val)=>{

        return val;
    })
}
this.Cancel = async()=>{

    await cancel.click();
    console.log('CancelButton');
};


}

module.exports = updateGratuity;