const { browser, element } = require("protractor")

var assignHelper = function(){

     var hierarchy = element(by.xpath('//a[@title="Catalog"]'));
     var gratuity = element(by.xpath('//a[@ng-reflect-router-link="administrative-category"]'));
    var folder = element(by.xpath('//mat-icon[@title="Folder"]'));
    var tab = element(by.cssContainingText('.mat-tab-label-content','Gratuities'));
    var assign = element(by.xpath('//a[@class="addBtn fa fa-add ng-star-inserted"]'));
    var dropdown = element(by.cssContainingText('.mat-form-field-infix','Select Gratuity'));
    // var select = element(by.cssContainingText('.mat-option-text',' Gratuity@20% '));
    var search = element(by.xpath('//input[@ng-reflect-placeholder="Search"]'));
    var select = element(by.xpath('//mat-pseudo-checkbox[@class="mat-option-pseudo-checkbox mat-pseudo-checkbox ng-star-inserted"]'));
   var proceed = element(by.xpath('//div[@class="cdk-overlay-backdrop cdk-overlay-transparent-backdrop cdk-overlay-backdrop-showing"]'));
   var proceed1 = element(by.cssContainingText('.mat-button-wrapper',' Proceed '));


// this.get= async()=>{

//     await browser.get('http://10.0.12.18:8011/administrative-category');
//     console.log('admin category');
// };

this.Hierarchy = async()=>{
    await hierarchy.click();
    console.log('Hierarchy');
};

this.Gratuity = async()=>{
    await gratuity.click();
    console.log('Gratuity');
};
this.Folder = async()=>{

    await folder.click();
    console.log('Folder');
};

this.Tab = async()=>{

    await tab.click();
    console.log('Tab');
};
this.Assign = async()=>{

    await assign.click();
    console.log('Assign');
};
this.Dropdown = async()=>{

    await dropdown.click();
    console.log('dropdown');
};

this.Search = async()=>{

    await search.sendKeys('vb963');
    console.log('search');

};
this.Select = async()=>{

    await select.click();
    console.log('select');
};
this.Proceed = async()=>{

    await proceed.click();
    console.log('proceed');
};
this.Proceed1 = async()=>{

    await proceed1.click();
    console.log('proceed');
};
}

module.exports = assignHelper;