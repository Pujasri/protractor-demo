var helper = function(){

var userName = element(by.xpath('//input[@formcontrolname="username"]'));
var password = element(by.xpath('//input[@formcontrolname="password"]'));
var login = element(by.xpath('//button[@ng-reflect-color="primary"]'));
var greeting = element(by.xpath('//snack-bar-container[@role="status"]'));
const EC = protractor.ExpectedConditions;

this.get = async() =>{

    await browser.get('http://10.0.12.18:8011/login');
    console.log('URL zPOS');
};

this.setUserName = async(name)=>{
    await userName.sendKeys(name);
    console.log('Username');
};

this.setPassword = async(pwd)=>{
    await password.sendKeys(pwd);
    console.log('Password');
};

this.loginButton = async()=>{

    await login.click();
    console.log('logged in')
};

this.waitForUrl = async()=>{

    await browser.wait(() => {
        return browser.wait(EC.urlContains("http://10.0.12.18:8011/hierarchy"));
      });
      console.log('hierarchy');
     


};

this.getgreeting = async()=>{

    return await greeting.getText().then((val)=>{

        console.log(val);
    })

};


};

module.exports = helper