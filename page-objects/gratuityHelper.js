const { element } = require("protractor");

var gratuityHelper = function(){

var hierarchy = element(by.xpath('//a[@title="Operations"]'));
var gratuity = element(by.xpath('//a[@ng-reflect-router-link="gratuity"]'));
var createGr = element(by.xpath('//fa-icon[@class="ng-fa-icon"]'));
var name = element(by.xpath('//input[@formcontrolname="Name"]'));
var code = element(by.xpath('//input[@formcontrolname="Code"]'));
var rate = element(by.xpath('//input[@formcontrolname="Rate"]'));
// var metricType = element(by.xpath('//mat-option[@ng-reflect-value="$"]'));
var create = element(by.xpath('//button[@color="primary"]'));
var greeting = element(by.xpath('//snack-bar-container[@role="status"]'));
var cancel = element(by.cssContainingText('.mat-button-wrapper','Cancel'));

// this.get = async() =>{

//     await browser.get('http://10.0.12.18:8011/gratuity');
//     console.log('Gratuity URL');
// };
this.Hierarchy = async()=>{
    await hierarchy.click();
    console.log('Hierarchy');
};

this.Gratuity = async()=>{
    await gratuity.click();
    console.log('Gratuity');
};

this.createGratuity = async()=>{

    await createGr.click();
    console.log('create window');
};

this.Name = async(gName)=>{

    await name.sendKeys(gName);
    console.log('Username');
};

this.Code = async(gCode)=>{

    await code.sendKeys(gCode);
    console.log('Code');
};
this.Rate = async(gRate)=>{

    await rate.sendKeys(gRate);
    console.log('Rate');
};
// this.MetricType = async()=>{

//     await metricType.click();
//     console.log('Metric Type');
// };
this.createButton = async()=>{

    await create.click();
    console.log('Create Button');
};

this.getGreetings = async()=>{

  return await greeting.getText().then((val)=>{
    console.log(val);
    
    });
    
};
this.Cancel = async()=>{

    await cancel.click();
    console.log('CancelButton');
};
}

module.exports = gratuityHelper;