
var HtmlReporter = require('protractor-beautiful-reporter');

exports.config = {
    framework: 'jasmine2',
    // seleniumAddress: 'http://localhost:4444/wd/hub',
    directConnect:true,

    capabilities: {
      'browserName': 'chrome',
      chromeOptions: {
        args: ['--disable-gpu']
      }
    },
    // specs: ["Test-Suite/*.spec.js",],
    specs: ["Test-Suite/*/*.spec.js",],
    // SELENIUM_PROMISE_MANAGER:false,

    jasmineNodeOpts: {
      showColors: true,
      defaultTimeoutInterval: 90000,
      isVerbose: true
    },

    onPrepare: () => {
      browser.manage().window().maximize();
      browser.manage().timeouts().implicitlyWait(5000);

      jasmine.getEnv().addReporter(new HtmlReporter({

        baseDirectory:'Reports/screenshots',
        screenshotsSubfolder:'Images 2.0'
      }).getJasmine2Reporter());
    }  
  
  }
  